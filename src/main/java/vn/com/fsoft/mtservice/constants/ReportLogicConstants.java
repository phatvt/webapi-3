package vn.com.fsoft.mtservice.constants;

/**
 * hungxoan
 */
public class ReportLogicConstants {

    public static final String IMPORT_COLLECTIONS = "importWSTCollection";
    public static final String IMPORT_COLLECTION_REQUIREMENTS =
            "importWSTCollectionRequirement";
    public static final String IMPORT_REQUIREMENTS = "importRequirement";

}
