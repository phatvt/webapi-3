package vn.com.fsoft.mtservice.object.entities;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;
import vn.com.fsoft.mtservice.util.Views;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 
 * @author hungxoan
 *
 */

@Entity(name = "MasterDataParameterEntity")
@Table(name = "master_data_parameters", schema = DatabaseConstants.SCHEMA)
public class MasterDataParameterEntity extends TrackingFieldEntity implements Serializable {

    @Id
	@Column(name = "id")
    @GeneratedValue(generator = "master_data_parameters_value_generator",
            strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "master_data_parameters_value_generator", schema = DatabaseConstants.SCHEMA,
            sequenceName = "master_data_parameters_seq")
    private Integer id;
    
    @Column(name = "master_category")
	private String masterCategory;
    
    @Column(name = "name")
    private String name;

    @Column(name = "type")
	private String type;
    
    @Column(name = "celldata")
    private String cellData; // value1, value2, value3, value4, value5
    
    @Column(name = "weight")
    private Integer weight;

    @Column(name = "is_required")
	private Boolean isRequired;

	@Column(name = "max_length")
	private Integer maxLength;

	@Column(name = "used")
	private Boolean used;
    
    public MasterDataParameterEntity() {
    }

	@JsonView(Views.Public.class)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonView(Views.Public.class)
	public String getMasterCategory() {
		return masterCategory;
	}

	public void setMasterCategory(String masterCategory) {
		this.masterCategory = masterCategory;
	}

	@JsonView(Views.Public.class)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonView(Views.Public.class)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonView(Views.Public.class)
	public String getCellData() {
		return cellData;
	}

	public void setCellData(String cellData) {
		this.cellData = cellData;
	}

	@JsonView(Views.Public.class)
	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	@JsonView(Views.Public.class)
	public Boolean getRequired() {
		return isRequired;
	}

	public void setRequired(Boolean required) {
		isRequired = required;
	}

	@JsonView(Views.Public.class)
	public Integer getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}

	@JsonView(Views.Hidden.class)
	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}
}
