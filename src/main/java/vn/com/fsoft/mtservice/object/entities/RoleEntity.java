package vn.com.fsoft.mtservice.object.entities;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;
import vn.com.fsoft.mtservice.util.Views;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author hungxoan
 *
 */
@Entity(name = "RoleEntity")
@Table(name = "roles", schema = DatabaseConstants.SCHEMA)
public class RoleEntity extends TrackingFieldEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "id")
	@GeneratedValue(generator = "roles_value_generator",
     		strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "roles_value_generator", schema = DatabaseConstants.SCHEMA,
			sequenceName = "roles_seq")
    private Integer id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description", length = 1000)
	private String description;
	
	@OneToMany(mappedBy = "role", fetch = FetchType.LAZY,
			cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
	@Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	private Set<AuthorityEntity> authorities = new HashSet<AuthorityEntity>();

	@ManyToMany(fetch = FetchType.LAZY,
			cascade ={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(schema = DatabaseConstants.SCHEMA, name = "profile_roles",
			joinColumns = {@JoinColumn(name = "role_id")},
			inverseJoinColumns = {@JoinColumn(name = "profile_id")})
	private Set<ProfileEntity> profiles = new HashSet<ProfileEntity>();
	
	public RoleEntity() {
		super();
	}

	@JsonView(Views.Public.class)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonView(Views.Public.class)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonView(Views.Public.class)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonView(Views.Public.class)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonView(Views.Public.class)
	public Set<AuthorityEntity> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<AuthorityEntity> authorities) {

	    for(AuthorityEntity authority : authorities) {
		    authority.setRole(this);
        }
	    this.authorities = authorities;
	}

	@JsonView(Views.Hidden.class)
	public Set<ProfileEntity> getProfiles() {
		return profiles;
	}

	public void setProfiles(Set<ProfileEntity> profiles) {
		this.profiles = profiles;
	}
}
