package vn.com.fsoft.mtservice.object.base;

import org.apache.commons.lang3.StringUtils;

import vn.com.fsoft.mtservice.util.NumberUtils;

/**
 * 
 * @author hungxoan
 *
 */
public class IncomingRequestContext {

    private Integer userId;
    private String token;

    public IncomingRequestContext() {
        super();
    }

    private IncomingRequestContext(Integer userId, String token) {

        this.userId = userId;
        this.token = token;
    }

    public static IncomingRequestContext getInstance(Integer userId, String token) {

        return new IncomingRequestContext(userId, token);
    }

    public static Boolean isWellForm(IncomingRequestContext context) {
        if(context == null) {
            return Boolean.FALSE;
        }

        if(NumberUtils.isEmpty(context.getUserId())) {
            return Boolean.FALSE;
        }

        if(StringUtils.isEmpty(context.getToken())) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    public IncomingRequestContext getInstance() {
        if(!isWellForm(this)) {
            return null;
        }

        return new IncomingRequestContext(userId, token);
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
