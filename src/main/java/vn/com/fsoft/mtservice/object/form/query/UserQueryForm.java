package vn.com.fsoft.mtservice.object.form.query;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author hungxoan
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserQueryForm extends RestQueryForm {

    private String userName;
    private String email;
    private String secret;
    private String fullName;
    private String phone;
    private String address;
    private String company;
    private Integer[] roles;

    public UserQueryForm() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer[] getRoles() {
        return roles;
    }

    public void setRoles(Integer[] roles) {
        this.roles = roles;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
