package vn.com.fsoft.mtservice.object.models;


import java.util.List;

import vn.com.fsoft.mtservice.object.base.ResultTransformer;

/**
 * 
 * @author hungxoan
 *
 */
public class UserAuthorityModel extends ResultTransformer{

    private String code;
    private String name;
    private String permission;
    private List<String> permissions;

    public UserAuthorityModel() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }
}
