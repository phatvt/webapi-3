package vn.com.fsoft.mtservice.object.entities;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.util.Views;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author hungxoan
 *
 */

@MappedSuperclass
public class TrackingFieldEntity extends HibernateRootEntity implements Serializable {

    @Column(name = "created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "edited_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date editedTime;

    @Column(name = "edited_by")
    private Integer editedBy;

    public TrackingFieldEntity() {
    }

    @JsonView(Views.Hidden.class)
    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @JsonView(Views.Hidden.class)
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @JsonView(Views.Hidden.class)
    public Date getEditedTime() {
        return editedTime;
    }

    public void setEditedTime(Date editedTime) {
        this.editedTime = editedTime;
    }

    @JsonView(Views.Hidden.class)
    public Integer getEditedBy() {
        return editedBy;
    }

    public void setEditedBy(Integer editedBy) {
        this.editedBy = editedBy;
    }
}
