package vn.com.fsoft.mtservice.object.form.query;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */

@JsonIgnoreProperties( ignoreUnknown = true)
public class RoleQueryForm extends RestQueryForm {

    private String code;
    private String name;
    private String description;

    private List<AuthorityQueryForm> authorities;

    public RoleQueryForm() {}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AuthorityQueryForm> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<AuthorityQueryForm> authorities) {
        this.authorities = authorities;
    }
}
