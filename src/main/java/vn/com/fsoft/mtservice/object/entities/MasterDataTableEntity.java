package vn.com.fsoft.mtservice.object.entities;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;
import vn.com.fsoft.mtservice.util.Views;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 
 * @author hungxoan
 *
 */

@Entity(name = "MasterDataTableEntity")
@Table(name = "master_data_table", schema = DatabaseConstants.SCHEMA)
public class MasterDataTableEntity extends TrackingFieldEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "master_data_table_value_generator",
            strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "master_data_table_value_generator", schema = DatabaseConstants.SCHEMA,
            initialValue = 11111, allocationSize = 5,
            sequenceName = "master_data_table_seq")
    private Integer id;
    
    @Column(name = "key")
    private String key;

    @Column(name = "name")
    private String name;

    @Column(name = "master_category")
    private String masterCategory;

    @Column(name = "description")
    private String description;

    @Column(name = "value1")
    private String value1;

    @Column(name = "value2")
    private String value2;

    @Column(name = "value3")
    private String value3;

    @Column(name = "value4")
    private String value4;

    @Column(name = "value5")
    private String value5;

    @Column(name = "parent")
    private Integer parent;

    @Column(name = "parent_name")
    private String parentName;

    @Column(name = "lock")
    private Boolean lock;

    public MasterDataTableEntity() {
    }

    @JsonView(Views.Public.class)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonView(Views.Public.class)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonView(Views.Public.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonView(Views.Public.class)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonView(Views.Public.class)
    public String getMasterCategory() {
		return masterCategory;
	}

	public void setMasterCategory(String masterCategory) {
		this.masterCategory = masterCategory;
	}

    @JsonView(Views.Public.class)
    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    @JsonView(Views.Public.class)
    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    @JsonView(Views.Public.class)
    public String getValue3() {
        return value3;
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    @JsonView(Views.Public.class)
    public String getValue4() {
        return value4;
    }

    public void setValue4(String value4) {
        this.value4 = value4;
    }

    @JsonView(Views.Public.class)
    public String getValue5() {
        return value5;
    }

    public void setValue5(String value5) {
        this.value5 = value5;
    }

    @JsonView(Views.Public.class)
    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    @JsonView(Views.Public.class)
    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    @JsonView(Views.Public.class)
    public Boolean getLock() {
        return lock;
    }

    public void setLock(Boolean lock) {
        this.lock = lock;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MasterDataTableEntity)){
            return false;
        }
        MasterDataTableEntity m = (MasterDataTableEntity) obj;
        if (this.id.equals(m.getId())) {
            return true;
        } else {
            return false;
        }
    }
}
