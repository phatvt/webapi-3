package vn.com.fsoft.mtservice.object.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author hungxoan
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationForm {

    private String userName;
    private String secret;

    public AuthenticationForm() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
