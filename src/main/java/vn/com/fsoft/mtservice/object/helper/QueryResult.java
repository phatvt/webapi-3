package vn.com.fsoft.mtservice.object.helper;


import java.util.List;

import vn.com.fsoft.mtservice.object.entities.HibernateRootEntity;

/**
 * 
 * @author hungxoan
 *
 * @param <E>
 */
public class QueryResult<E extends HibernateRootEntity> {

    private Integer rowCount;
    private List<E> entityList;

    public QueryResult(Integer rowCount, List<E> entityList) {
        this.rowCount = rowCount;
        this.entityList = entityList;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public List<E> getEntityList() {
        return entityList;
    }

    public void setEntityList(List<E> entityList) {
        this.entityList = entityList;
    }

}
