package vn.com.fsoft.mtservice.object.constant.enumeration;

/**
 * hungxoan
 */
public enum FunctionEnum {

    USER("user_func"),
    ROLE("role_func"),
    MASTER_DATA("master_data_func"),
    REPORT_TEMPLATE("report_template_func"),

    ASSESSMENT_STANDARD("assessment_standard_func"),
    ASSESSMENT_REQUIREMENT("assessment_requirement_func"),
    WST_COLLECTION("wst_collection_func"),
    PROJECT("project_func"),
    LOGIN_HISTORY("login_history_func"),
    DATAPOINT("data_point_func"),
    TOOLTIP("tooltip_func");

    private String functionName;

    FunctionEnum(String functionName) {
        this.functionName = functionName;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
}
