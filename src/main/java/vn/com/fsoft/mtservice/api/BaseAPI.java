package vn.com.fsoft.mtservice.api;

import org.springframework.http.HttpStatus;

import vn.com.fsoft.mtservice.constants.HttpConstant;

/**
 * 
 * @author hungxoan
 *
 */
public class BaseAPI {

    public BaseAPI() {}

    protected HttpStatus getStatus(String code) {
        if(code.equals(HttpConstant.CODE_SUCCESS)) {
            return HttpStatus.OK;
        }

        if(code.equals(HttpConstant.CODE_BAD_REQUEST)) {
            return HttpStatus.BAD_REQUEST;
        }

        if(code.equals(HttpConstant.CODE_NOT_FOUND)) {
            return HttpStatus.NOT_FOUND;
        }

        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
