package vn.com.fsoft.mtservice.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import vn.com.fsoft.mtservice.bean.data.RoleRepo;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.controller.command.RoleCommandController;
import vn.com.fsoft.mtservice.controller.query.RoleQueryController;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.QueryResponseStatus;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.Status;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.object.constant.enumeration.FunctionEnum;
import vn.com.fsoft.mtservice.object.entities.FunctionPermissionEntity;
import vn.com.fsoft.mtservice.object.entities.MasterDataTableEntity;
import vn.com.fsoft.mtservice.object.entities.RoleEntity;
import vn.com.fsoft.mtservice.object.form.RoleForm;
import vn.com.fsoft.mtservice.object.models.PermissionModel;
import vn.com.fsoft.mtservice.object.models.StaticAuthorityModel;
import vn.com.fsoft.mtservice.services.validator.RoleValidator;
import vn.com.fsoft.mtservice.util.IncomingRestUtils;
import vn.com.fsoft.mtservice.util.JacksonUtils;
import vn.com.fsoft.mtservice.util.Views;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */

@RestController("roleAPI")
@RequestMapping("/api/role")
public class RoleAPI extends BaseAPI{

    @Autowired
    private RoleCommandController commandController;

    @Autowired
    private RoleQueryController queryController;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private RoleValidator roleValidator;

    @GetMapping(value = "/_findOne/{roleCode}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> findOneImplementation(@PathVariable String roleCode,
                                                        RequestEntity<RoleForm> requestEntity) {

        RepoStatus<List<RoleEntity>> status = roleRepo.findByCode(roleCode);

        if(!status.getCode().equals(HttpConstant.CODE_SUCCESS)) {
            return new ResponseEntity(JacksonUtils.java2Json(new Status(status.getCode(),
                    status.getMessage())), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(JacksonUtils.java2Json(new QueryResponseStatus<>(HttpConstant.CODE_SUCCESS,
                "found", status.getObject()), Views.Public.class), HttpStatus.OK);
    }

    @PostMapping(value = "/_query",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> queryImplementation(RequestEntity<RoleForm> requestEntity) {

        ValidationStatus validationStatus = roleValidator.reject(requestEntity, "query");
        if(validationStatus != null) {
            return queryController.reject(validationStatus);
        }

        IncomingRequestContext requestContext = IncomingRestUtils.getContext(requestEntity);

        return queryController.query(requestContext, FunctionEnum.ROLE, requestEntity);
    }

    @GetMapping(value ="/_permissions",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAllPermissions(RequestEntity<RoleForm> requestEntity) {

        RepoStatus<List<MasterDataTableEntity>> roleStatus = roleRepo.getPermissions();

        if(roleStatus.getObject() == null) {
            return new ResponseEntity<String>(JacksonUtils.
                    java2Json(new QueryResponseStatus("404", "not found")), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<String>(JacksonUtils.
            java2Json(new QueryResponseStatus("200", "found", roleStatus.getObject()),
                    Views.Public.class), HttpStatus.OK);
    }

    @GetMapping(value ="/_authorities",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAllAuthorities(RequestEntity<RoleForm> requestEntity) {

        RepoStatus<List<FunctionPermissionEntity>> status = roleRepo.getAuthorityList();

        if(CollectionUtils.isEmpty(status.getObject())) {
            return new ResponseEntity<String>(JacksonUtils
                    .java2Json(new QueryResponseStatus("404", "not found")),
                    HttpStatus.OK);
        }

        List<FunctionPermissionEntity> authorities = status.getObject();

        // transform
        StaticAuthorityModel currentRole = null;
        StaticAuthorityModel trackingRole = null;
        List<StaticAuthorityModel> roleList = new ArrayList<StaticAuthorityModel>();
        PermissionModel permission = null;
        List<PermissionModel> permissionList = new ArrayList<PermissionModel>();

        Integer trackingRoleId = 0;
        Integer currentRoleId = 0;

        if(authorities != null && authorities.size() > 0) {
            for(FunctionPermissionEntity entity: authorities) {

                currentRoleId = entity.getFunction().getId();

                currentRole = new StaticAuthorityModel();
                currentRole.setId(entity.getFunction().getId());
                currentRole.setCode(entity.getFunction().getKey());
                currentRole.setName(entity.getFunction().getName());
                currentRole.setMasterCategory(entity.getFunction().getMasterCategory());

                permission = new PermissionModel();
                permission.setId(entity.getPermission().getId());
                permission.setCode(entity.getPermission().getKey());
                permission.setName(entity.getPermission().getName());

                if(trackingRoleId == 0) {
                    trackingRoleId = currentRoleId;
                    trackingRole = currentRole;
                }

                if(trackingRoleId == currentRoleId) {

                    permissionList.add(permission);
                } else {

                    trackingRoleId = currentRoleId;
                    trackingRole.setPermissions(permissionList);
                    roleList.add(trackingRole);
                    trackingRole = currentRole;

                    permissionList = new ArrayList<PermissionModel>();
                    permissionList.add(permission);
                }
            }

            if(roleList != null && roleList.size() > 0) {
                if (trackingRoleId != roleList.get(roleList.size() - 1).getId()) {
                    trackingRole.setPermissions(permissionList);
                    roleList.add(trackingRole);
                }
            }
        }

        return new ResponseEntity<String>(JacksonUtils
                .java2Json(new QueryResponseStatus("200", "found", roleList)),
                HttpStatus.OK);
    }

    @PutMapping(value = "/_index",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> saveImplementation(RequestEntity<RoleForm> requestEntity) {

        ValidationStatus validationStatus = roleValidator.reject(requestEntity, "command");
        if(validationStatus != null) {
            return commandController.reject(validationStatus);
        }

        RoleForm form = requestEntity.getBody();

        String type = form.getT();
        IncomingRequestContext requestContext = IncomingRestUtils.getContext(requestEntity);

        if (type.equals("n")) {

            return commandController.save(requestContext, FunctionEnum.ROLE, requestEntity);
        }

        return commandController.update(requestContext, FunctionEnum.ROLE, requestEntity);
    }

    @DeleteMapping(value = "/_delete/{id}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> deleteImplementation(@PathVariable("id") Integer id,
                                                       RequestEntity<RoleForm> requestEntity) {

        IncomingRequestContext requestContext = IncomingRestUtils.getContext(requestEntity);
        return commandController.delete(requestContext, id, FunctionEnum.ROLE, requestEntity);
    }

    @PostMapping(value = "/_bulk",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> deleteBulkImplementation(RequestEntity<RoleForm> requestEntity) {

        // validate
        ValidationStatus validationStatus = roleValidator.reject(requestEntity, "bulk");
        if(validationStatus != null) {
            return commandController.reject(validationStatus);
        }

        IncomingRequestContext requestContext = IncomingRestUtils.getContext(requestEntity);
        return commandController.bulk(requestContext, FunctionEnum.ROLE, requestEntity);
    }
}
