package vn.com.fsoft.mtservice.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.ObjectNotFoundException;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.util.CollectionUtils;

import vn.com.fsoft.mtservice.object.constant.enumeration.VisibilityEnum;
import vn.com.fsoft.mtservice.object.entities.HibernateRootEntity;


/**
 * 
 * @author hungxoan
 *
 */
public class HibernateUtils {

    public static <T extends HibernateRootEntity> T getEntity(HibernateTemplate hibernateTemplate,
                                                               Class<T> clazz, Integer id) {

        T t = null;

        try {
            t = hibernateTemplate.get(clazz, id);
        } catch (DataAccessException ex) {
            // log ex
            return null;
        } catch(ObjectNotFoundException oex) {
            // log ex
            return null;
        }

        return t;
    }

    public static <T extends HibernateRootEntity> T loadEntity(HibernateTemplate hibernateTemplate,
                                                               Class<T> clazz, Integer id) {

        T t = null;

        try {
            t = hibernateTemplate.get(clazz, id);
        } catch (DataAccessException ex) {
            // log ex
            return null;
        } catch(ObjectNotFoundException oex) {
            // log ex
            return null;
        }

        return t;
    }

    public static <T extends HibernateRootEntity> T loadLockedMasterEntity(
            HibernateTemplate hibernateTemplate, Integer id) {

        String hql = "select m from MasterDataTableEntity m where m.lock = true";
        List<T> tList = null;

        try {

            tList = (List<T>) hibernateTemplate.find(hql);
        } catch (DataAccessException ex) {
            // log ex
            return null;
        }

        if(CollectionUtils.isEmpty(tList)) {
            return null;
        }

        return tList.get(0);
    }

    public static <T extends HibernateRootEntity> T loadVisibilityTemplate(HibernateTemplate hibernateTemplate,
                                                                           VisibilityEnum visibilityEnum) {

        String status = visibilityEnum.getStatus();

        if(StringUtils.isBlank(status)) {
            return null;
        }

        status = status.toLowerCase();

        List<T> tList = null;
        String hql = "select v from VisibilityTemplate v where v.status = :status";
        try {
            tList = (List<T>) hibernateTemplate.findByNamedParam(hql, "status", status);
        } catch (DataAccessException ex) {
            // log ex
            return null;
        }

        if(CollectionUtils.isEmpty(tList)) {
            return null;
        }

        return tList.get(0);
    }


}
