package vn.com.fsoft.mtservice.bean.data;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import vn.com.fsoft.mtservice.bean.manager.ApplicationConfigManager;
import vn.com.fsoft.mtservice.bean.manager.MasterDataManager;
import vn.com.fsoft.mtservice.constants.CommonConstants;
import vn.com.fsoft.mtservice.constants.DatabaseConstants;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.ResultTransformer;
import vn.com.fsoft.mtservice.object.constant.enumeration.PermissionEnum;
import vn.com.fsoft.mtservice.object.entities.ProfileEntity;
import vn.com.fsoft.mtservice.object.entities.RoleEntity;
import vn.com.fsoft.mtservice.object.entities.UserEntity;
import vn.com.fsoft.mtservice.object.entities.UserSecretHistoryEntity;
import vn.com.fsoft.mtservice.object.form.UserForm;
import vn.com.fsoft.mtservice.object.form.command.UserCommandForm;
import vn.com.fsoft.mtservice.object.form.query.UserQueryForm;
import vn.com.fsoft.mtservice.object.helper.QueryCallBack;
import vn.com.fsoft.mtservice.object.helper.QueryResult;
import vn.com.fsoft.mtservice.object.helper.TransformQueryResult;
import vn.com.fsoft.mtservice.object.helper.TransformerQueryCallBack;
import vn.com.fsoft.mtservice.object.models.RecordExistModel;
import vn.com.fsoft.mtservice.object.models.UserAuthorityModel;
import vn.com.fsoft.mtservice.services.tokenizing.ShortPasswordService;
import vn.com.fsoft.mtservice.util.CryptoUtils;
import vn.com.fsoft.mtservice.util.HibernateUtils;
import vn.com.fsoft.mtservice.util.NumberUtils;

import javax.transaction.Transactional;
import java.util.*;

/**
 * 
 * @author hungxoan
 *
 */
@Repository("userRepo")
public class UserRepo extends BaseRepo<UserEntity, ResultTransformer, UserForm, UserCommandForm> {

    @Value("${record.per.page}")
    private Integer recordPerPage;

    @Value("${reset.password.value}")
    private String resetPasswordValue;

    @Value("${security.hashing.algorithm}")
    private String hashingAlgorithm;

    @Value("${app.context.frontend}")
    private String appContextFrontend;

    @Value("${login.uri}")
    private String loginUri;

    @Autowired
    private MasterDataManager masterDataManager;

    @Autowired
    private ShortPasswordService shortPasswordService;

    @Autowired
    private ApplicationConfigManager appConfigManager;

    private HibernateTemplate hibernateTemplate;

    public UserRepo(@Autowired SessionFactory sessionFactory) {
        super(sessionFactory);
        hibernateTemplate = getHibernateTemplates();
    }

    @Override
    public RepoStatus<UserEntity> findById(Integer id) {

        UserEntity entity = null;
        try {

            entity = hibernateTemplate.get(UserEntity.class, id);

        } catch (DataAccessException ex) {
            // log exc
            return null;
        }

        return new RepoStatus(HttpConstant.CODE_SUCCESS, "found", entity);
    }

    @Override
    public RepoStatus<List<UserEntity>> findByCode(String userName) {

        String hql = "select u from UserEntity u where u.userName = :userName and u.used = :used";

        List<UserEntity> entityList = null;
        try {
            entityList = (List<UserEntity>) hibernateTemplate.findByNamedParam(hql,
                    new String[]{"userName", "used"},
                    new Object[] {userName, Boolean.TRUE});
        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        } catch (ObjectNotFoundException ex) {
            // log ex
            return new RepoStatus("404",
                    "No user linked to this user name");
        }

        if(CollectionUtils.isEmpty(entityList)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        return new RepoStatus<List<UserEntity>>(HttpConstant.CODE_SUCCESS, "found", entityList);
    }

    public RepoStatus<UserEntity> findByCredential(String userName, String password) {

        if (StringUtils.isBlank(userName)) {
            return null;
        }

        if (StringUtils.isBlank(password)) {
            return null;
        }

        String hql = "select u from UserEntity u where userName = :userName and secret = :secret ";
        hql += " and used = :used";
        String[] params = new String[] {"userName", "secret", "used"};
        Object[] values = new Object[] {userName, password, Boolean.TRUE};

        UserEntity user = null;
        List<UserEntity> userList = null;
        try {
            userList = (List<UserEntity>) hibernateTemplate.findByNamedParam(hql, params, values);
        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus<UserEntity>(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        if(CollectionUtils.isEmpty(userList)) {
            return null;
        }

        user = userList.get(0);
        return new RepoStatus<UserEntity>(HttpConstant.CODE_SUCCESS, "found", user);
    }

    public RepoStatus<UserEntity> findUser(String userName, String hashPassword) {

        if (StringUtils.isEmpty(userName)) {
            return null;
        }

        if (StringUtils.isEmpty(hashPassword)) {
            return null;
        }

        String hql = "select u from UserEntity u left join fetch u.profile p " +
                "where u.userName = :userName and u.secret = :secret and u.used = :used";
        String[] params = new String[] {"userName", "secret", "used"};
        Object[] values = new Object[] {userName, hashPassword, Boolean.TRUE};

        UserEntity user = null;
        List<UserEntity> userList = null;
        try {
            userList = (List<UserEntity>) hibernateTemplate.findByNamedParam(hql, params, values);
        } catch (DataAccessException ex) {
            // log ex
        }

        if(userList == null || userList.size() == 0) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        user = userList.get(0);

        return new RepoStatus(HttpConstant.CODE_SUCCESS, "found", user);
    }

    public RepoStatus<UserEntity> findByEmail(String email) {

        String hql = "Select u from UserEntity u left join fetch u.profile p where u.email = :email";
        hql += " and u.used = :used";
        List<UserEntity> users = null;
        try {
            users = (List<UserEntity>) hibernateTemplate
                    .findByNamedParam(hql, new String[] {"email", "used"},
                            new Object[] {email, Boolean.TRUE});

        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");
        } catch (ObjectNotFoundException ex) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "not found");
        }

        if (CollectionUtils.isEmpty(users)) {
            return new RepoStatus<>(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        return new RepoStatus<>(HttpConstant.CODE_SUCCESS, "found", users.get(0));
    }

    public RepoStatus<UserEntity> findByEmail(String email, String userName) {

        String hql = "Select u from UserEntity u left join fetch u.profile p where u.email = :email";
        hql += " and u.used = :used and u.userName != :userName";
        List<UserEntity> users = null;
        try {
            users = (List<UserEntity>) hibernateTemplate
                    .findByNamedParam(hql, new String[] {"email", "used", "userName"},
                            new Object[] {email, Boolean.TRUE, userName});

        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");
        } catch (ObjectNotFoundException ex) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "not found");
        }

        if (CollectionUtils.isEmpty(users)) {
            return new RepoStatus<>(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        return new RepoStatus<>(HttpConstant.CODE_SUCCESS, "found", users.get(0));
    }

    public RepoStatus<List<UserAuthorityModel>> getUserAuthorities(Integer userId) {

        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT F.KEY AS code, F.NAME AS name, string_agg(P.NAME, ' , ') AS permission");
        sqlBuilder.append(" FROM " + DatabaseConstants.SCHEMA + ".AUTHORITY A ");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".ROLES R ON A.ROLE_ID = R.ID ");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".PROFILE_ROLES PFR ON PFR.ROLE_ID = R.ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".PROFILES PF ON PF.ID= PFR.PROFILE_ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".USERS U ON PF.USER_ID = U.ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".MASTER_DATA_TABLE F ON F.ID = A.FUNCTION_ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".MASTER_DATA_TABLE P ON P.ID = A.ROLE_PERMISSION_ID");
        sqlBuilder.append(" WHERE U.ID = :userId AND U.used = :used");
        sqlBuilder.append(" GROUP BY F.KEY, F.NAME");

        String[] keySet = new String[] {"userId", "used"};
        Object[] valueSet = new Object[] {userId, Boolean.TRUE};

        TransformerQueryCallBack<UserAuthorityModel> transformerQueryCallBack = new
                TransformerQueryCallBack<UserAuthorityModel>(
                sqlBuilder.toString(), keySet, valueSet, 0, 0,
                UserAuthorityModel.class, "UserAuthority"
        );

        List<UserAuthorityModel> resultList = (List<UserAuthorityModel>) hibernateTemplate
                .execute(transformerQueryCallBack);


        String[] permissions = null;
        List<String> permissionList = null;
        String permission = CommonConstants.EMPTY;
        int permissionSize = 0;
        List<UserAuthorityModel> authorities = new ArrayList<UserAuthorityModel>();

        if(!CollectionUtils.isEmpty(resultList)) {

            for (UserAuthorityModel authority : resultList) {

                permission = authority.getPermission();
                if (StringUtils.isNotEmpty(permission)) {
                    permissions = permission.split(",");
                }

                if (ArrayUtils.isNotEmpty(permissions)) {
                    permissionList = Arrays.asList(permissions);
                    authority.setPermissions(permissionList);
                    authorities.add(authority);
                }

                permission = CommonConstants.EMPTY;
            }
        }

        if(CollectionUtils.isEmpty(authorities)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND, "not found");
        }

        return new RepoStatus(HttpConstant.CODE_SUCCESS, "found", authorities);
    }

    public RepoStatus<Boolean> isUserOwnRole(PermissionEnum permission, Integer userId) {

        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT 1 AS result  ");
        sqlBuilder.append(" FROM " + DatabaseConstants.SCHEMA + ".AUTHORITY A ");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".ROLES R ON A.ROLE_ID = R.ID ");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".PROFILE_ROLES PFR ON PFR.ROLE_ID = R.ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".PROFILES PF ON PF.ID= PFR.PROFILE_ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".USERS U ON PF.USER_ID = U.ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".MASTER_DATA_TABLE F ON F.ID = A.FUNCTION_ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".MASTER_DATA_TABLE P ON P.ID = A.ROLE_PERMISSION_ID");
        sqlBuilder.append(" WHERE U.ID = :userId and P.NAME = :permission and U.used = :used ");

        String[] keySet = new String[] {"userId", "permission", "used"};
        Object[] valueSet = new Object[] {userId, permission.getPermissionType(), Boolean.TRUE};

        TransformerQueryCallBack<RecordExistModel> hbmSQLQueryCallBack = new
                TransformerQueryCallBack<RecordExistModel>(
                sqlBuilder.toString(), keySet, valueSet, 0, 0,
                RecordExistModel.class, "RecordExist"
        );

        List<RecordExistModel> resultList = (List<RecordExistModel>)
                hibernateTemplate.execute(hbmSQLQueryCallBack);

        if(CollectionUtils.isEmpty(resultList)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "not found", Boolean.FALSE);
        }

        return new RepoStatus(HttpConstant.CODE_SUCCESS, "found", Boolean.TRUE);
    }

    public RepoStatus<Boolean> isUserOwnRoleList(Integer userId) {

        String projectItemFullPermission = appConfigManager.get("ProjectItemFullPermission");
        String[] roleList = projectItemFullPermission.split(",");

        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT EXISTS(SELECT 1 FROM " + DatabaseConstants.SCHEMA + ".AUTHORITY A");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".ROLES R ON A.ROLE_ID = R.ID ");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".PROFILE_ROLES PFR ON PFR.ROLE_ID = R.ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".PROFILES PF ON PF.ID= PFR.PROFILE_ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".USERS U ON PF.USER_ID = U.ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".MASTER_DATA_TABLE F ON F.ID = A.FUNCTION_ID");
        sqlBuilder.append(" LEFT JOIN " + DatabaseConstants.SCHEMA + ".MASTER_DATA_TABLE P ON P.ID = A.ROLE_PERMISSION_ID");
        sqlBuilder.append(" WHERE U.ID = :userId and U.used = :used and r.code in :roleList ) as result");

        String[] keySet = new String[] {"userId", "roleList", "used"};
        Object[] valueSet = new Object[] {userId, roleList, Boolean.TRUE};

        TransformerQueryCallBack<RecordExistModel> hbmSQLQueryCallBack = new
                TransformerQueryCallBack<RecordExistModel>(
                sqlBuilder.toString(), keySet, valueSet, 0, 0,
                RecordExistModel.class, "RecordExist"
        );

        List<RecordExistModel> resultList = (List<RecordExistModel>)
                hibernateTemplate.execute(hbmSQLQueryCallBack);

        if(resultList.get(0).getResult().equals(false)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "not found", Boolean.FALSE);
        }

        return new RepoStatus(HttpConstant.CODE_SUCCESS, "found", Boolean.TRUE);
    }

    public RepoStatus<List<RoleEntity>> getRoles() {

        String hql = "select r from RoleEntity r";
        List<RoleEntity> roleList = null;
        try {
            roleList = (List<RoleEntity>) hibernateTemplate.find(hql);
        } catch (DataAccessException ex) {
            // log ex
            return null;
        }

        if(CollectionUtils.isEmpty(roleList)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,"not found");
        }

        return new RepoStatus(HttpConstant.CODE_SUCCESS, "found", roleList);
    }

    @Override
    public RepoStatus<QueryResult<UserEntity>> query(UserForm form) {

        if (null == form) {
            return null;
        }

        Integer page = form.getP();
        if (null == page || 0 == page) {
            page = 1;
        }

        Integer pSize = form.getpSize();
        if (null == pSize || 0 == pSize) {
            pSize = recordPerPage;
        }

        UserQueryForm formQuery = form.getQuery();
        String hqlSelect = "select u from UserEntity u left join fetch u.profile p";
        hqlSelect += " left join fetch p.roles r ";
        String hqlFinal = "";
        String hqlWhere = "";

        Map<String, Object> parameterMap = new HashMap<String, Object>();
        Integer[] roleIds;
        List<Integer> roleIdList = new ArrayList<Integer>();
        if (null != formQuery) {
            if (StringUtils.isNotEmpty(formQuery.getUserName())) {
                if (hqlWhere.length() > 0) {
                    hqlWhere += " and ";
                }
                hqlWhere += " u.userName like :userName ";
                parameterMap.put("userName", CommonConstants.PERCENT + formQuery.getUserName() +
                        CommonConstants.PERCENT);
            }

            if (StringUtils.isNotEmpty(formQuery.getEmail())) {
                if (hqlWhere.length() > 0) {
                    hqlWhere += " and ";
                }
                hqlWhere += " u.email like :email ";
                parameterMap.put("email", CommonConstants.PERCENT + formQuery.getEmail() +
                        CommonConstants.PERCENT);
            }

            if (StringUtils.isNotEmpty(formQuery.getFullName())) {
                if (hqlWhere.length() > 0) {
                    hqlWhere += " and ";
                }
                hqlWhere += " p.fullName like :fullName ";
                parameterMap.put("fullName", CommonConstants.PERCENT + formQuery.getFullName() +
                        CommonConstants.PERCENT);
            }

            if (StringUtils.isNotEmpty(formQuery.getAddress())) {
                if (hqlWhere.length() > 0) {
                    hqlWhere += " and ";
                }
                hqlWhere += " p.address like :address ";
                parameterMap.put("address", CommonConstants.PERCENT + formQuery.getAddress() + CommonConstants.PERCENT);
            }

            if (StringUtils.isNotEmpty(formQuery.getPhone())) {
                if (hqlWhere.length() > 0) {
                    hqlWhere += " and ";
                }
                hqlWhere += " p.phone like :phone ";
                parameterMap.put("phone", CommonConstants.PERCENT + formQuery.getPhone() + CommonConstants.PERCENT);
            }

            if (StringUtils.isNotEmpty(formQuery.getCompany())) {
                if (hqlWhere.length() > 0) {
                    hqlWhere += " and ";
                }
                hqlWhere += " p.company like :company ";
                parameterMap.put("company", CommonConstants.PERCENT + formQuery.getCompany() + CommonConstants.PERCENT);
            }

            if(formQuery.getRoles() != null && formQuery.getRoles().length > 0) {

                roleIds = formQuery.getRoles();
                roleIdList = Arrays.asList(roleIds);
                if (hqlWhere.length() > 0) {
                    hqlWhere += " and ";
                }
                hqlWhere += " r.id in :roleIds ";
                parameterMap.put("roleIds", roleIdList);
            }

            // set used in user query
            if(hqlWhere.length() > 0) {
                hqlWhere += " and ";
            }
            hqlWhere += " u.used = :used";
            parameterMap.put("used", Boolean.TRUE);
        }

        hqlFinal = hqlSelect +
                (!StringUtils.isEmpty(hqlWhere) ? (" where " + hqlWhere + " and u.used = :used") :
                        " where u.used = :used") + " order by u.createdTime desc";

        String key = "";
        Object value = null;
        String[] keySet = new String[parameterMap.size() + 1];
        Object[] valueSet = new Object[parameterMap.size() + 1];
        int idx = 0;
        for(Map.Entry<String, Object> entry : parameterMap.entrySet()) {

            key = entry.getKey();
            value = entry.getValue();
            keySet[idx] = key;
            valueSet[idx] = value;
            idx++;
        }

        keySet[parameterMap.size()] = "used";
        valueSet[parameterMap.size()] = Boolean.TRUE;

        QueryCallBack hbmQueryCallBack = new QueryCallBack<UserEntity>(
                hqlFinal, keySet, valueSet, (page-1)*pSize, pSize
        );

        List<UserEntity> entityList = (List<UserEntity>) hibernateTemplate
                .execute(hbmQueryCallBack);

        if(CollectionUtils.isEmpty(entityList)) {
            return new RepoStatus<>("202", "not found");
        }

        Integer rowCount = hbmQueryCallBack.getRowCount();
        QueryResult<UserEntity> queryResult = new QueryResult<UserEntity>(rowCount - 1, entityList);

        return new RepoStatus("200", "found", queryResult);
    }

    @Override
    public RepoStatus<TransformQueryResult<ResultTransformer>> transformQuery(UserForm form) {
        return null;
    }

    @Override
    protected RepoStatus<Object> objectQuery(UserForm form) {
        return null;
    }

    @Override
    @Transactional
    public RepoStatus<Integer> save(IncomingRequestContext context, UserCommandForm commandForm) {

        UserEntity userEntity = null;
        RoleEntity roleEntity = null;
        Set<RoleEntity> roleEntitySet = new HashSet<RoleEntity>();

        // load role
        if (commandForm.getRoles() != null && commandForm.getRoles().length > 0) {

            for (Integer roleId : commandForm.getRoles()) {
                try {
                    roleEntity = HibernateUtils.getEntity(hibernateTemplate, RoleEntity.class, roleId);
                } catch (DataAccessException ex) {
                    // log ex
                    return new RepoStatus<Integer>(HttpConstant.CODE_SERVER_ERROR,
                            "Server got error. Please wait a moment and try it again later");
                }
                roleEntitySet.add(roleEntity);
            }
        }

        ProfileEntity profileEntity = new ProfileEntity();
        profileEntity.setFullName(commandForm.getFullName());
        profileEntity.setAddress(commandForm.getAddress());
        profileEntity.setPhone(commandForm.getPhone());
        profileEntity.setCompany(commandForm.getCompany());
        profileEntity.setRoles(roleEntitySet);
        profileEntity.setCreatedBy(context.getUserId());
        profileEntity.setCreatedTime(new Date());

        userEntity = new UserEntity();
        String generatedSecret = shortPasswordService.needAPassword(context.getUserId());
        String hashSecret = CryptoUtils.digest(generatedSecret, hashingAlgorithm);
        userEntity.setUserName(commandForm.getUserName());
        userEntity.setEmail(commandForm.getEmail());
        userEntity.setFirstLogin(Boolean.TRUE);
        userEntity.setSecret(hashSecret);
        userEntity.setUsed(Boolean.TRUE);
        profileEntity.setUserId(userEntity);
        userEntity.setProfile(profileEntity);
        userEntity.setCreatedBy(context.getUserId());
        userEntity.setCreatedTime(new Date());

        try {

            hibernateTemplate.save(userEntity);
        } catch(DataAccessException ex) {
            // log exc
            return new RepoStatus<Integer>(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");
        }

        String loginUrl = appContextFrontend + loginUri;

        Map<String, String> model = new HashMap<String, String>();
        model.put("fullName", commandForm.getFullName());
        model.put("userName", commandForm.getUserName());
        model.put("generatedSecret", generatedSecret);
        model.put("loginUrl", loginUrl);

        //HungPV comment this point
//        AddUserEmailThread thread = new AddUserEmailThread(mailer, emailFactory,
//                commandForm.getEmail(), model);
//
//        thread.start();
        return new RepoStatus<Integer>(HttpConstant.CODE_SUCCESS, "acknowledged",
                userEntity.getId());
    }

    @Override
    @Transactional
    public RepoStatus<Integer> update(IncomingRequestContext context, UserCommandForm commandForm) {

        String hql = "select u from UserEntity u left join fetch u.profile p left join p.roles r";
        hql += " where u.id = :id";

        List<UserEntity> entityList = null;
        try {
            entityList = (List<UserEntity>) hibernateTemplate
                    .findByNamedParam(hql, "id", commandForm.getId());
        } catch (DataAccessException ex) {

            return new RepoStatus(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");
        } catch (ObjectNotFoundException ex) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "No user linked to this identifier");
        }

        UserEntity entity = null;
        if(CollectionUtils.isEmpty(entityList)) {

            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "No user linked to this identifier");
        }

        entity = entityList.get(0);

        if (null != entity) {
            RoleEntity roleEntity = null;
            Set<RoleEntity> roleEntitySet = new HashSet<RoleEntity>();

            if (commandForm.getRoles() != null && commandForm.getRoles().length > 0) {

                for (Integer roleId : commandForm.getRoles()) {
                    try {
                        roleEntity = hibernateTemplate.get(RoleEntity.class, roleId);
                    } catch (DataAccessException ex) {
                        // log ex
                        return new RepoStatus<Integer>(HttpConstant.CODE_SERVER_ERROR,
                                "Server got error. Please wait a moment and try it again later");
                    }
                    roleEntitySet.add(roleEntity);
                }
            }

            ProfileEntity profileEntity = entity.getProfile();

            profileEntity.setFullName(commandForm.getFullName());
            profileEntity.setAddress(commandForm.getAddress());
            profileEntity.setPhone(commandForm.getPhone());
            profileEntity.setCompany(commandForm.getCompany());
            profileEntity.setRoles(roleEntitySet);
            profileEntity.setEditedBy(context.getUserId());
            profileEntity.setEditedTime(new Date());

            entity.setId(commandForm.getId());
            entity.setUserName(commandForm.getUserName());
            entity.setEmail(commandForm.getEmail());
            entity.setProfile(profileEntity);
            entity.setEditedBy(context.getUserId());
            entity.setEditedTime(new Date());

            try {
                hibernateTemplate.update(entity);
            } catch (DataAccessException ex) {
                // log exc
                return new RepoStatus<Integer>(HttpConstant.CODE_SERVER_ERROR,
                        "Server got error. Please wait a moment and try it again later");
            }

            return new RepoStatus<Integer>(HttpConstant.CODE_SUCCESS, "acknowledged",
                    entity.getId());
        }

        return new RepoStatus<Integer>(HttpConstant.CODE_NOT_FOUND,
                "No user at record identification existed", entity.getId());
    }

    @Transactional
    public RepoStatus<Integer> retrieveSecret(Integer userId, String newSecret) {

        RepoStatus<UserEntity> repostatus = null;
        try {
            repostatus = this.findById(userId);

        } catch (DataAccessException ex) {

            // log ex
            return new RepoStatus<Integer>(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");

        } catch (ObjectNotFoundException ex) {
            // log ex
        }

        if (repostatus == null){
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,"not found");
        }

        if(repostatus.getObject() == null) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,"not found");
        }


        UserEntity user = repostatus.getObject();
        user.setSecret(newSecret);
        user.setFirstLogin(Boolean.FALSE);
        user.setEditedTime(new Date());
        user.setEditedBy(userId);

        String oldSecret = user.getSecret();

        RepoStatus<UserSecretHistoryEntity> rStatus = this.getUserSecretHistory(userId);

        if (rStatus == null) {
            return new RepoStatus<Integer>(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");
        }

        UserSecretHistoryEntity history = null;

        if (rStatus.getObject() == null) {

            history = new UserSecretHistoryEntity();
            history.setCreatedTime(new Date());
            history.setCreatedBy(userId);
        } else {

            history = rStatus.getObject();
            history.setEditedTime(new Date());
            history.setEditedBy(userId);
        }

        history.setUser(user);

        // Compare new password with 3 latest changed password
        if (newSecret.equals(history.getSecret1())
                || newSecret.equals(history.getSecret2())
                || newSecret.equals(history.getSecret3())) {

            return new RepoStatus<Integer>(HttpConstant.CODE_FORBIDDEN,
                    "The password should not be equal to the recently 3 passwords");
        }

        history.setSecret3(history.getSecret2());
        history.setSecret2(history.getSecret1());
        history.setSecret1(oldSecret);

        try {
            hibernateTemplate.update(user);
            hibernateTemplate.saveOrUpdate(history);
        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus<Integer>(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");
        }

        return new RepoStatus<Integer>(HttpConstant.CODE_SUCCESS, "acknowledged", user.getId());
    }

    @Override
    @Transactional
    public RepoStatus<Boolean> delete(Integer id) {

        if (NumberUtils.isEmpty(id)) {

            return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                    "User identifier is not valid", Boolean.FALSE);
        }

        String hql = "select u from UserEntity u left join fetch u.profile p left join fetch p.roles r";
        hql += " where u.id = :userId";
        List<UserEntity> userEntityList = null;
        UserEntity user = null;

        try {
            userEntityList = (List<UserEntity>) hibernateTemplate.findByNamedParam(hql, "userId", id);
        } catch (DataAccessException ex) {
            // log ex
            return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                    "Server got error. Please wait a moment and try it again later",
                    Boolean.FALSE);
        }

        if(CollectionUtils.isEmpty(userEntityList)) {
            return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                    "This user identifier is not existed", Boolean.FALSE);
        }

        user = userEntityList.get(0);

        if(null != user) {

            // check system admin
            if(isSystemAdmin(user.getUserName())) {
                return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                        "Unable to delete system admin user account",
                        Boolean.FALSE);
            }

            // remove all role that granted to user
            Set<RoleEntity> roleEntitySet = user.getProfile().getRoles();
            for(RoleEntity roleEntity : roleEntitySet) {
                user.getProfile().getRoles().remove(roleEntity);
            }

            // disable user
            user.setUsed(Boolean.FALSE);
            try {
                hibernateTemplate.update(user);
            } catch (DataAccessException ex) {
                // log exc
                return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                        "Server got error. Please wait a moment and try it again later",
                        Boolean.FALSE);
            } catch (HibernateException ex) {
                // log exc
                return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                        "This user is referenced from many objects. " +
                                "Please remove all the refenrences before delete.",
                        Boolean.FALSE);
            }

            return new RepoStatus<Boolean>(HttpConstant.CODE_SUCCESS, "acknowledged",
                    Boolean.TRUE);
        }

        return new RepoStatus<Boolean>(HttpConstant.CODE_NOT_FOUND,
                "No user at record identification existed", Boolean.FALSE);
    }

    @Override
    @Transactional
    public RepoStatus<Boolean> deleteBulk(List<Integer> ids) {

        if (ids == null || ids.size() == 0) {

            return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                    "The list of user identification is not valid", Boolean.FALSE);
        }

        String hql = "select u from UserEntity u left join fetch u.profile p left join fetch p.roles" +
                " where u.id in :ids";
        List<UserEntity> entityList = (List<UserEntity>) hibernateTemplate
                .findByNamedParam(hql, "ids", ids);

        if(!CollectionUtils.isEmpty(entityList)) {

            for(UserEntity entity : entityList) {

                if(isSystemAdmin(entity.getUserName())) {
                    return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                            "There's a system admin user account in the list. Unable to delete",
                            Boolean.FALSE);
                }

                // remove all role that granted to user
                Set<RoleEntity> roleEntitySet = entity.getProfile().getRoles();
                for(RoleEntity roleEntity : roleEntitySet) {
                    entity.getProfile().getRoles().remove(roleEntity);
                }

                // disable user
                entity.setUsed(Boolean.FALSE);

                try {
                    hibernateTemplate.update(entity);
                } catch (DataAccessException ex) {
                    // log exc
                    return new RepoStatus<Boolean>(HttpConstant.CODE_BAD_REQUEST,
                            "Server got error. Please wait a moment and try it again later",
                            Boolean.FALSE);
                }
            }
        }

        return new RepoStatus<Boolean>(HttpConstant.CODE_SUCCESS, "acknowledged",
                Boolean.TRUE);
    }

    @Transactional
    public RepoStatus<UserSecretHistoryEntity> getUserSecretHistory(Integer userId) {

        String hql = "Select u From UserSecretHistoryEntity u left join fetch u.user s " +
                "Where s.id = :userId";
        try {
            List<UserSecretHistoryEntity> list = (List<UserSecretHistoryEntity>)
                    hibernateTemplate.findByNamedParam(hql, "userId", userId);

            if (list != null && !list.isEmpty()) {

                UserSecretHistoryEntity u = list.get(0);
                return new RepoStatus<UserSecretHistoryEntity>(HttpConstant.CODE_SUCCESS,
                        "found", u);
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return new RepoStatus<>(HttpConstant.CODE_NOT_FOUND, "not found");
    }

    @Transactional
    public RepoStatus<UserEntity> changePassword(Integer userId, String oldSecret, String newSecret) {

        if (NumberUtils.isEmpty(userId)) {
            return new RepoStatus("409", "User identifier should be not empty");
        }

        UserEntity userEntity = null;

        try {

            userEntity = hibernateTemplate.get(UserEntity.class, userId);
        } catch (DataAccessException ex) {
            return new RepoStatus<UserEntity>(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");
        }

        if(userEntity.getUsed() == null || !userEntity.getUsed()) {
            return new RepoStatus<UserEntity>(HttpConstant.CODE_FORBIDDEN,
                    "This user is disabled or invalid");
        }

        String currentSecret = userEntity.getSecret();

        if (!currentSecret.equals(oldSecret)) {
            return new RepoStatus(HttpConstant.CODE_NOT_FOUND,
                    "The current password is wrong");
        }

        if (StringUtils.contains(newSecret, userEntity.getUserName())) {
            return new RepoStatus<UserEntity>(HttpConstant.CODE_BAD_REQUEST,
                    "Your New Password is invalid. Please try again.");
        }

        if (currentSecret.equals(newSecret)) {
            return new RepoStatus<UserEntity>(HttpConstant.CODE_NOT_FOUND,
                    "The new password cannot be the same with the current password.");
        }

        userEntity.setSecret(newSecret);
        userEntity.setFirstLogin(Boolean.FALSE);

        String hql = "select s from UserSecretHistoryEntity s left join fetch s.user u where u.id = :id";
        List<UserSecretHistoryEntity> secretHistories = null;
        UserSecretHistoryEntity secretHistory = null;
        try {
            secretHistories = (List<UserSecretHistoryEntity>) hibernateTemplate
                    .findByNamedParam(hql, "id", userEntity.getId());
        } catch (DataAccessException ex) {
            return new RepoStatus(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");
        }

        if(!CollectionUtils.isEmpty(secretHistories)) {
            secretHistory = secretHistories.get(0);
            secretHistory.setSecret1(newSecret);
        }

        try {
            hibernateTemplate.update(userEntity);
            if(secretHistory != null) {
                hibernateTemplate.update(secretHistory);
            }
        } catch (DataAccessException ex) {
            return new RepoStatus<UserEntity>(HttpConstant.CODE_SERVER_ERROR,
                    "Server got error. Please wait a moment and try it again later");
        }

        return new RepoStatus<UserEntity>(HttpConstant.CODE_SUCCESS,
                "acknowledged");
    }

    private Boolean isSystemAdmin(String userName) {

        String systemAdminUser = appConfigManager.get("SystemAdminAccount");
        if(userName.equals(systemAdminUser)) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }
}
