package vn.com.fsoft.mtservice.bean.interceptor;


import vn.com.fsoft.mtservice.bean.sercurity.CredentialTokenService;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.constant.enumeration.PermissionEnum;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * @author hungxoan
 *
 */

@Component("tokenInterceptor")
public class TokenInterceptor implements Interceptor {

    @Value("${security.hashing.algorithm}")
    private String hashingAlgorithm;

    @Autowired
    private CredentialTokenService credentialTokenService;

    @Override
    public Boolean isAcknowledged(IncomingRequestContext context) {

        if(context == null) {
            return Boolean.FALSE;
        }

        Integer userId = context.getUserId();
        String token = context.getToken();

        if(userId == null || userId == 0) {
            return Boolean.FALSE;
        }

        if(StringUtils.isBlank(token)) {
            return Boolean.FALSE;
        }

        // check token
        if(!credentialTokenService.isMatch(userId, token)) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    @Override
    public Boolean isAcknowledged(IncomingRequestContext context, PermissionEnum permissionEnum) {
        return null;
    }
}
