package vn.com.fsoft.mtservice.bean.sercurity;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import vn.com.fsoft.mtservice.constants.CommonConstants;
import vn.com.fsoft.mtservice.object.base.ClientCredential;
import vn.com.fsoft.mtservice.object.entities.CredentialTokenEntity;
import vn.com.fsoft.mtservice.util.CryptoUtils;
import vn.com.fsoft.mtservice.util.NumberUtils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author hungxoan
 *
 */

@Component("credentialTokenService")
public class CredentialTokenService {

    // only MD5, SHA-1 and SHA-256 is supported
    @Value("${security.hashing.algorithm}")
    private String hashingAlgorithm;

    // key is hash(userId)
    private Map<Integer, String> tokens = new HashMap<Integer, String>();
    private Map<Integer, ClientCredential> tokenInfos = new HashMap<Integer, ClientCredential>();

    public CredentialTokenService(@Autowired SessionFactory sessionFactory) {
        loadFromDatabase(sessionFactory);
    }

    private void loadFromDatabase(SessionFactory sessionFactory) {

        // broadcast to clustering nodes for persist the token to database.


        // get token from database
        String hql = "select T from CredentialTokenEntity T";
        HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
        List<CredentialTokenEntity> entityList = null;
        try {
            entityList = (List<CredentialTokenEntity>) hibernateTemplate.find(hql);
        } catch (DataAccessException ex) {

        } catch (ObjectNotFoundException ex) {

        }

        if(!CollectionUtils.isEmpty(entityList)) {

            Integer userId = null;
            String token = CommonConstants.EMPTY;
            String nonce = CommonConstants.EMPTY;
            ClientCredential credential = null;

            for(CredentialTokenEntity entity : entityList) {
                userId = entity.getUserId();
                token = entity.getToken();
                nonce = entity.getNonce();
                credential = new ClientCredential(userId, token, nonce);
                tokens.put(userId, token);
                tokenInfos.put(userId, credential);
            }
        }
    }

    public ClientCredential generateToken(Integer userId) {

        if(!isWellForm(userId)) {
            return null;
        }

        SecureRandom secureRandom = new SecureRandom();
        String sRand1 = new BigInteger(130, secureRandom).toString(32);
        String sRand2 = new BigInteger(140, secureRandom).toString(32);

        String nonce = sRand1 + "_" + sRand2;
        String sSequence = userId + "_" + nonce;
        String token = CryptoUtils.digest(sSequence, hashingAlgorithm);

        return new ClientCredential(userId, token, nonce);
    }
    
    public Boolean isMatch(Integer userId, String currentToken) {
    	
    	if(!isWellForm(userId)) {
    		return Boolean.FALSE;
    	}
    	
    	String serverToken = CommonConstants.EMPTY;
    	if(tokens.containsKey(userId)) {

            serverToken = tokens.get(userId);
        }
    	
    	if(StringUtils.isEmpty(serverToken)) {
    		return Boolean.FALSE;
    	}
    	
    	if(!serverToken.equals(currentToken)) {
    		return Boolean.FALSE;
    	}

    	return Boolean.TRUE;
    }

    public synchronized ClientCredential signIn(Integer userId) {

        if(NumberUtils.isEmpty(userId)) {
            return null;
        }

        ClientCredential clientCredential = generateToken(userId);

        if(clientCredential != null && StringUtils.isNotEmpty(clientCredential.getToken())) {
            tokens.put(userId, clientCredential.getToken());
        }

        return clientCredential;
    }
    
    public synchronized ClientCredential consumeToken(Integer userId, String token) {

        if(StringUtils.isBlank(token)) {
            return null;
        }

        ClientCredential clientCredential = null;
        if(tokens.containsKey(userId)) {

            if(tokens.get(userId).equals(token)) {

                clientCredential = generateToken(userId);
            } else {

                return null;
            }
        }

        if(clientCredential == null || StringUtils.isBlank(clientCredential.getToken())) {
            return null;
        }

        tokens.put(userId, clientCredential.getToken());
        tokenInfos.put(userId, clientCredential);
    	return clientCredential;
    }

    public ClientCredential getClientCredential(Integer userId) {

        return tokenInfos.get(userId);
    }

    private Boolean isWellForm(Integer userId) {

        if(userId == null || userId == 0) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    public String get(Integer userId) {
        if(tokens.containsKey(userId)) {
            return tokens.get(userId);
        }

        return null;
    }
}