package vn.com.fsoft.mtservice.services.tokenizing;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import vn.com.fsoft.mtservice.constants.CommonConstants;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author hungxoan
 *
 */
@Component("shortPasswordService")
public class ShortPasswordService {

    private static ShortPasswordService _INSTANCE;
    private Map<Integer, String> passwordDatabase = new HashMap<Integer, String>();
    private LoadingCache<Integer, String> passwords;
    private final String INIT_SEQUENCE = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private final Integer TOKEN_LENGTH = 8;

    public ShortPasswordService() {
        passwords = CacheBuilder.newBuilder()
                .concurrencyLevel(4)
                .weakKeys()
                .maximumSize(10000)
                .expireAfterWrite(24, TimeUnit.HOURS)
                .build(new CacheLoader<Integer, String>() {
                    @Override
                    public String load(Integer userId) throws Exception {
                        return getFromDatabase(userId);
                    }
                });
    }

    public String getFromDatabase(Integer userId) {
        return passwordDatabase.get(userId);
    }

    private String generatePassword() {

        SecureRandom SECURE_RANDOM = new SecureRandom();
        StringBuilder sb = new StringBuilder( TOKEN_LENGTH );
        for( int i = 0; i < TOKEN_LENGTH; i++ )
            sb.append(INIT_SEQUENCE.charAt( SECURE_RANDOM.nextInt(INIT_SEQUENCE.length())));
        return sb.toString();
    }

    public String needAPassword(Integer userId) {

        String password = generatePassword();
        passwordDatabase.put(userId, password);
        return password;
    }

    public Boolean isValid(Integer userId, String password) {

        if(vn.com.fsoft.mtservice.util.NumberUtils.isEmpty(userId) || StringUtils.isBlank(password)) {
            return Boolean.FALSE;
        }

        String cachedPassword = CommonConstants.EMPTY;
        try {
            cachedPassword = passwords.get(userId);
        } catch (ExecutionException ex) {

        }

        if(StringUtils.isBlank(cachedPassword)) {
            return Boolean.FALSE;
        }

        if(!cachedPassword.equals(password)) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    public void changePassword(Integer userId) {
        passwordDatabase.remove(userId);
    }
}
