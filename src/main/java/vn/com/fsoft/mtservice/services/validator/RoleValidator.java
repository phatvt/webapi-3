package vn.com.fsoft.mtservice.services.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.com.fsoft.mtservice.bean.data.RoleRepo;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.constants.ValidatorConstants;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.object.entities.RoleEntity;
import vn.com.fsoft.mtservice.object.form.RoleForm;
import vn.com.fsoft.mtservice.object.form.command.RoleCommandForm;
import vn.com.fsoft.mtservice.object.form.query.RoleQueryForm;
import vn.com.fsoft.mtservice.util.NumberUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author hungxoan
 *
 */

@Component("roleValidator")
public class RoleValidator extends BaseValidator<RoleForm> {

    @Autowired
    private RoleRepo roleRepo;

    @Override
    protected ValidationStatus rejectValue(RoleForm form, String section) {
        ValidationStatus validationStatus = null;
        RoleCommandForm commandForm = null;
        RoleQueryForm queryForm = null;


        if (null != form) {

            commandForm = form.getModel();
            queryForm = form.getQuery();
            // check required fields

            if (section.equals("command")) {

                if(StringUtils.isBlank(form.getT())) {
                    return new ValidationStatus("400",
                            "Type of the form should not be empty");
                }

                switch (form.getT()) {
                    case "n":

                        // check empty
                        validationStatus = rejectIfEmpty(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        // check role code is not existed
                        validationStatus = rejectIfCodeExisted(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        // check code is wellformed or not
                        validationStatus = rejectIfCodeNotWellform(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfExceedLength(commandForm);
                        if (validationStatus != null) {
                            return validationStatus;
                        }

                        break;

                    case "u":

                        // check empty
                        validationStatus = rejectIfEmpty(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        // check empty
                        validationStatus = rejectIfIDEmpty(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        // check role id is existing
                        validationStatus = rejectIfIdNotExisted(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfExceedLength(commandForm);
                        if (validationStatus != null) {
                            return validationStatus;
                        }

                        break;

                    default:
                        break;
                }
            } else if(section.equals("query")) {

                validationStatus = rejectIfQueryFormUndefined(queryForm);
                if(validationStatus != null) {
                    return validationStatus;
                }

            } else if(section.equals("deleteBulk")) {


            }
        } else {
            // form is null
            validationStatus = new ValidationStatus("400", "form data is invalid.");
            return validationStatus;
        }

        return null;
    }

    //check roleCommandForm is empty
    private ValidationStatus rejectIfEmpty(RoleCommandForm commandForm) {

        if(commandForm == null) {
            return new ValidationStatus("400", "Form data is undefined.");
        }



        if (StringUtils.isEmpty(commandForm.getCode())) {

            return new ValidationStatus("400", "Role code should not be empty");
        }

        if (StringUtils.isEmpty(commandForm.getName())) {

            return new ValidationStatus("400", "Role name should not be empty");
        }

        return null;
    }

    private ValidationStatus rejectIfIDEmpty(RoleCommandForm commandForm) {

        if (NumberUtils.isEmpty(commandForm.getId())) {

            return new ValidationStatus("400", "Role identifier should not be empty");
        }

        return null;
    }

    private ValidationStatus rejectIfCodeExisted(RoleCommandForm commandForm) {

        RepoStatus<List<RoleEntity>> repoStatus = roleRepo.findByCode(commandForm.getCode());

        if(repoStatus != null) {
            if(repoStatus.getObject() != null) {
                return new ValidationStatus("404",
                        "This code is existing in the system. Unable to add");
            }
        } else {
            return new ValidationStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        }

        return null;
    }

    private ValidationStatus rejectIfCodeNotWellform(RoleCommandForm commandForm) {

        Pattern pattern = Pattern.compile(ValidatorConstants.CODE_REGEX);
        Matcher matcher = pattern.matcher(commandForm.getCode());

        if(!matcher.find()) {
            return new ValidationStatus("404",
                    "This code is not well-formed. Please choose another one");
        }

        return null;
    }

    private ValidationStatus rejectIfIdNotExisted(RoleCommandForm commandForm) {

        if(NumberUtils.isEmpty(commandForm.getId())) {
            return new ValidationStatus("400",
                    "Record identifier should not be empty");
        }

        RepoStatus<RoleEntity> repoStatus = roleRepo.findById(commandForm.getId());

        if(repoStatus != null) {
            if(repoStatus.getObject() == null) {
                return new ValidationStatus("404",
                        "No collection linked to this identifier");
            }
        } else {
            return new ValidationStatus("500",
                    "Server got error. Please wait a moment and try it again later.");
        }

        return null;
    }

    private ValidationStatus rejectIfCommandFormUndefined(RoleCommandForm commandForm) {

        if(commandForm == null) {
            return new ValidationStatus("400", "Command form data is undefined.");
        }

        return null;

    }

    private ValidationStatus rejectIfQueryFormUndefined(RoleQueryForm queryForm) {
        if(queryForm == null) {
            return new ValidationStatus("400", "Query form data is undefined.");
        }

        return null;
    }

    private ValidationStatus rejectIfExceedLength(RoleCommandForm commandForm) {

        if (StringUtils.length(commandForm.getName()) > 50){
            return new ValidationStatus(HttpConstant.CODE_BAD_REQUEST, "Name field should not exceed 50 characters.");
        }

        if (StringUtils.length(commandForm.getDescription()) > 1000) {
            return new ValidationStatus(HttpConstant.CODE_BAD_REQUEST,
                    "Description field should not exceed 1000 characters.");
        }

        return null;
    }
}
