package vn.com.fsoft.mtservice.services.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.object.form.AuthenticationForm;
import vn.com.fsoft.mtservice.object.form.UserForm;

/**
 * 
 * @author hungxoan
 *
 */

@Component("authenticationValidator")
public class AuthenticationValidator extends BaseValidator<UserForm> {

    @Override
    protected ValidationStatus rejectValue(UserForm form, String section) {

        AuthenticationForm authenForm = form.getAuthentication();

        if(authenForm == null) {
            return new ValidationStatus(HttpConstant.CODE_NOT_ACCEPTABLE,
                    "Authentication form is undefined");
        }

        String userName = authenForm.getUserName();
        String secret = authenForm.getSecret();

        if(StringUtils.isEmpty(userName)) {
            return new ValidationStatus(HttpConstant.CODE_NOT_ACCEPTABLE,
                    "User Name should not be empty");
        }

        if(StringUtils.isEmpty(secret)) {
            return new ValidationStatus(HttpConstant.CODE_NOT_ACCEPTABLE,
                    "User secret should not be empty");
        }

        if(secret.length() < 64) {
            return new ValidationStatus(HttpConstant.CODE_NOT_ACCEPTABLE,
                    "User secret is not in correct format");
        }

        return null;
    }


}
