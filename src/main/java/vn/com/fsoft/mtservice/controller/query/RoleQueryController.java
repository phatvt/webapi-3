package vn.com.fsoft.mtservice.controller.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;

import vn.com.fsoft.mtservice.bean.data.RoleRepo;
import vn.com.fsoft.mtservice.bean.interceptor.RoleCheckingInterceptor;
import vn.com.fsoft.mtservice.bean.interceptor.TokenInterceptor;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.QueryResponseStatus;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.Status;
import vn.com.fsoft.mtservice.object.entities.RoleEntity;
import vn.com.fsoft.mtservice.object.form.RoleForm;
import vn.com.fsoft.mtservice.object.helper.QueryResult;
import vn.com.fsoft.mtservice.util.JacksonUtils;
import vn.com.fsoft.mtservice.util.Views;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */

@Component("roleQueryController")
@DependsOn({"tokenInterceptor", "roleCheckingInterceptor"})
public class RoleQueryController extends QueryControllerTemplate<RoleForm> {

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    public RoleQueryController(TokenInterceptor tokenInterceptor,
                                 RoleCheckingInterceptor roleCheckingInterceptor) {

        setTokenInterceptor(tokenInterceptor);
        setRoleCheckingInterceptor(roleCheckingInterceptor);
    }

    @Override
    protected String getDefinition(IncomingRequestContext context,
                                   RequestEntity<RoleForm> requestEntity) {
        return null;
    }

    @Override
    protected String getDefinition(IncomingRequestContext context, Integer id,
                                   RequestEntity<RoleForm> requestEntity) {
        return null;
    }

    @Override
    protected String findOneDefinition(IncomingRequestContext context, String key,
                                       RequestEntity<RoleForm> requestEntity) {
        return null;
    }

    @Override
    protected String queryDefinition(IncomingRequestContext context,
                                     RequestEntity<RoleForm> requestEntity) {

        List<RoleEntity> entityList = null;
        Integer rowCount = 0;
        RoleForm form = requestEntity.getBody();

        RepoStatus<QueryResult<RoleEntity>> repoStatus = roleRepo.query(form);

        if(repoStatus == null) {
            return JacksonUtils.java2Json(new Status("500",
                    "Server got error. Please wait a moment and try it again later."));
        }

        if(repoStatus.getObject() != null &&
                repoStatus.getCode().equals(HttpConstant.CODE_SUCCESS)) {

            return JacksonUtils.java2Json(new QueryResponseStatus("200",
                            "found", repoStatus.getObject().getRowCount(),
                            repoStatus.getObject().getEntityList()),
                    Views.Public.class);
        }

        return JacksonUtils.java2Json(new Status(repoStatus.getCode(),
                repoStatus.getMessage()));
    }
}
