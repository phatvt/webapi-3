package vn.com.fsoft.mtservice.controller.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;

import vn.com.fsoft.mtservice.bean.data.UserRepo;
import vn.com.fsoft.mtservice.bean.interceptor.RoleCheckingInterceptor;
import vn.com.fsoft.mtservice.bean.interceptor.TokenInterceptor;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.object.base.CommandResponseStatus;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.Status;
import vn.com.fsoft.mtservice.object.form.UserForm;
import vn.com.fsoft.mtservice.object.form.command.UserCommandForm;
import vn.com.fsoft.mtservice.util.JacksonUtils;
import vn.com.fsoft.mtservice.util.Views;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */

@Component("userCommandController")
@DependsOn({"tokenInterceptor", "roleCheckingInterceptor"})
public class UserCommandController extends CommandControllerTemplate<UserForm>  {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    public UserCommandController(TokenInterceptor tokenInterceptor,
                                 RoleCheckingInterceptor roleCheckingInterceptor) {

        setTokenInterceptor(tokenInterceptor);
        setRoleCheckingInterceptor(roleCheckingInterceptor);
    }

    @Override
    protected String saveDefinition(IncomingRequestContext context,
                                    RequestEntity<UserForm> requestEntity) {

        UserForm form = requestEntity.getBody();
        UserCommandForm commandForm = form.getModel();

        RepoStatus<Integer> repoStatus = userRepo.save(context, commandForm);

        if(repoStatus == null) {
            return JacksonUtils.java2Json(new Status("500",
                    "Server got error. Please wait a moment and try it again later."));
        }

        if(repoStatus.getObject() != null &&
                repoStatus.getObject().equals(HttpConstant.CODE_SUCCESS)) {

            return JacksonUtils.java2Json(new CommandResponseStatus("200",
                    "acknowledged", repoStatus.getObject()), Views.Public.class);
        }

        return JacksonUtils.java2Json(new Status(repoStatus.getCode(),
                repoStatus.getMessage()));
    }

    @Override
    protected String updateDefinition(IncomingRequestContext context,
                                      RequestEntity<UserForm> requestEntity) {
        UserForm form = requestEntity.getBody();
        UserCommandForm commandForm = form.getModel();

        RepoStatus<Integer> repoStatus = userRepo.update(context, commandForm);

        if(repoStatus == null) {
            return JacksonUtils.java2Json(new Status("500",
                    "Server got error. Please wait a moment and try it again later."));
        }

        if(repoStatus.getObject() != null &&
                repoStatus.getObject().equals(HttpConstant.CODE_SUCCESS)) {

            return JacksonUtils.java2Json(new CommandResponseStatus("200",
                    "acknowledged", repoStatus.getObject()), Views.Public.class);
        }

        return JacksonUtils.java2Json(new Status(repoStatus.getCode(),
                repoStatus.getMessage()));
    }

    @Override
    protected String deleteDefinition(IncomingRequestContext context, Integer id,
                                      RequestEntity<UserForm> requestEntity) {

        RepoStatus<Boolean> repoStatus = userRepo.delete(id);

        if(repoStatus == null) {
            return JacksonUtils.java2Json(new Status("500",
                    "Server got error. Please wait a moment and try it again later."));
        }

        if(repoStatus.getObject() != null &&
                repoStatus.getObject().equals(HttpConstant.CODE_SUCCESS)) {

            return JacksonUtils.java2Json(new Status("200",
                    "acknowledged"), Views.Public.class);
        }

        return JacksonUtils.java2Json(new Status(repoStatus.getCode(),
                repoStatus.getMessage()));
    }

    @Override
    protected String bulkDefinition(IncomingRequestContext context,
                                    RequestEntity<UserForm> requestEntity) {

        UserForm form = requestEntity.getBody();
        UserCommandForm commandForm = form.getModel();

        List<Integer> ids = commandForm.getIds();
        RepoStatus<Boolean> repoStatus = userRepo.deleteBulk(ids);

        if(repoStatus == null) {
            return JacksonUtils.java2Json(new Status("500",
                    "Server got error. Please wait a moment and try it again later."));
        }

        if(repoStatus.getObject() != null &&
                repoStatus.getObject().equals(HttpConstant.CODE_SUCCESS)) {

            return JacksonUtils.java2Json(new Status("200",
                    "acknowledged"), Views.Public.class);
        }

        return JacksonUtils.java2Json(new Status(repoStatus.getCode(),
                repoStatus.getMessage()));
    }
}
